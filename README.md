
# NATUREX _ test technique

## Objectif

Réaliser une interface front selon le PSD fourni.

## Techno

ReactJS, CSS et JS

## Dossiers et fichiers

/node_modules: modules et librairies installés via NPM
/public
    /js 
        main.js     - code JS du menu Ticky
    /medias
        /langs      - drapeau pour le select langue
        /sliders    - Les images du slider
        *.png|*.jpg - Les autres différents éléments graphiques
    /styles
        main.css    - Fichier style général
index.html          - Fichier HTML Root
 * /src
*     /components
*        index.js    - index de tous les composants 
*        Header.js   - composant react affichant le header
*        LangagesSelect  - composant react affichant le selecteur des langues
*        Menu        - composant react affichant le menu
*       Sliders     - composant react affichant les sliders "who we are, what we do, how we do it"
*    /datas          
*        langs.js    - source datas des langs (drapeaux et iso)
*        sliders.js  - source datas des sliders (images, textes)
*   App.js          - APP.js 
*    index.js        - index with ReactDOM.render root
   
## Remarques

J'ai placé les médias dans le dossier public par commodité et cohérence globale, bien qu'il est possible de les inclures via des imports dans le fichier js correspond.
import en from '[PATH]/langs/en.png'; par exemple.

Idem pour les CSS.
Mais il est possible d'inclure un CSS dans un composant. 
import "[path]component.css"  
ou même en concevant un object contenant les instructions styles:
const ComponentTextStyles = {
    color: '#FFF',
    fontSize: 22
};

Ma préférences est de regrouper les composants et leur css dans un sous dossier:
* /components
*    /Menu
*        Menu.js
*        Menu.css
*       ...

Par exemple cela permet de mieux maintenir le code et de rendre réutilisable et modulable le composant.   
J'ai mis un component exemple dans le dossier des components: "ExempleComponent"         

Mais sur cette partie de mono page, j'ai choisi d'afficher un fichier style standard dans le dossier public/styles

J'ai aussi choisi de ne pas utiliser ici jQuery, mais de réserver les codes JS à Vanilla. Par ce que j'aime bien Vanilla, qu'il est efficace. Mais on peut inclure jQuery dans les projets ReactJS.

J'ai ajouté une zone en bas, pour bien montrer que le sticky fonctionne.
j'ai déduis aussi que si menu en haut devenait sticky en scrollant vers le bas, il redeviendrait en bas si on réaffiche le header. Dans le cas contraire il est simple de conserver le menu en haut, en modifiant le js.

Les Sliders devant occupés tout l'écan disponible (hors menu), me donne l'impression d'être plus grand que sur le PSD où elles n'occupent qu'une partie de l'écran. L'ajustement des fond n'a pas été simple.

J'ai rencontré des soucis pour extraire certains textes avec photoshop ou même xd design (psd importé). Soit par manque de police sur mon ordinateur, soit simplement car impossible d'accéder à la couche (layout) du texte.
Souvent dans ces cas, je demande l'assistance du créatif (designer) mais cette fois, je me suis débrouillée autrement.

Pour la version mobile, n'ayant pas de PSD, je me suis basée sur le visuel jpg, en ajustant (absence du bloc actuelité entre autre) et en désactivant l'effet sur les sliders.

J'ai organisé mon code et les dossiers pour avoir une certaine cohérence et maintenir facilement.
Mais je serais preneuse d'autres organisations, si vous avez l'habitude de faire autrement. Je suis très souple et agile pour cela.

## Live Démo

Le site est aussi disponible en live ici:

https://lasour.sytes.net:805/


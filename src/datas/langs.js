
const { PUBLIC_URL  } = process.env; 

export const langs = {
    'en' : { flag: `${PUBLIC_URL}/medias/langs/en.png`},
    'fr' : { flag: `${PUBLIC_URL}/medias/langs/fr.png`},
}

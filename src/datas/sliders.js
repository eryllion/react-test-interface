
const { PUBLIC_URL  } = process.env; 
export const sliders = [
    { 
        backgroundImage: `url('${PUBLIC_URL}/medias/sliders/WHO_WE_ARE.jpg')`,
        label: 'WHO WE ARE',
        labelImage: process.env.PUBLIC_URL + '/medias/sliders/slidetext1.png', 
        listing:[]
    },
    { 
        backgroundImage: `url('${PUBLIC_URL}/medias/sliders/WHAT_WE_DO.jpg')` ,
        label: 'WHAT WE DO',
        labelImage: process.env.PUBLIC_URL + '/medias/sliders/slideText2.png',
        listing:[
            {
                defaut: true, 
                menu: 'Sourcing',
                text: `Au coeur des ressources naturelles locales, Naturex explore chaque jour la biosphère et extrait plus 600 familles d'ingrédients naturels.`   
            },
            {
                menu: 'manufacturing',
                text: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce faucibus lectus at nunc feugiat aliquet`   
            },
            {
                menu: 'innovation',
                text: `Nunc quam sem, consectetur nec gravida vitae, lobortis sed nisl. Mauris euismod ultricies suscipit. Aliquam nec ipsum sed magna rutrum mollis`
            },
            {
                menu: 'products',
                text: `NDonec mattis nulla a ante porta, ut cursus mi ornare. Vestibulum ante ipsum primis in faucibus orci luctus et ultrices posuere cubilia curae`
            }
        ]
    },
    { 
        backgroundImage: `url('${PUBLIC_URL}/medias/sliders/HOW_WE_DO_IT.jpg')` , 
        label: 'HOW WE DO IT',
        labelImage: process.env.PUBLIC_URL + '/medias/sliders/slideText3.png',
        listing:[]
    }

];
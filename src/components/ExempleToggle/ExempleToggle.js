import React from 'react';
import './ExempleToggle.css';

export class ExempleToggle extends React.Component {
    constructor( props ) {
        this.state   = {
            yellow: false 
        };
    }
    render() {
        const { yellow } = this.state; 
        return (<div id="bg" className={( yellow ? 'bgYellow' : '' ) } 
        onClick={ () => this.Toggle() }  > 
            <span>CLICK HERE</span>

        </div>);
    }
    Toggle() {
        const yellow = ( this.state.yellow ? false : true )
        this.setState({
            yellow
        })
    }
}

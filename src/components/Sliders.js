import React  from "react";

// import sliders datas from datas folder
import { sliders } from '../datas/sliders.js'; 

export class Sliders extends React.Component {
    constructor( props ) {
        super( props );
        this.state = {
            current: false
        };
    
    }
    render() {
       
        return (<div id="sliders-block" >
            {sliders.map( ({ backgroundImage, label, labelImage, listing }) => {
                return (<div key={label} className="slider" onClick={(e) => this.Slide(e)} style={{ backgroundImage }} >
                    <span className="slider-content" >
                       <img src={labelImage} alt={label} />
                       <div className="slider-separator"></div>
                       {this.renderInfos({ label,  listing})}
                   </span>
               </div>);
               })} 
        </div>)
    }
    /**
     * 
     * @param {object} { label, listing }  
     * @returns React Element
     */
    renderInfos({ label, listing }) {
        if( listing.length === 0 ) return false; 
        let contentTextes = []; 
        let liElements = [];
        /** J'ai choisi de faire ainsi pour ne faire qu'une seule boucle */
        for( let  i in listing) {
            const { defaut = false , menu, text } = listing[i]; 
            liElements.push(<li key={menu} onMouseOver={(e) => this.Showtext( e , label,  menu ) }>
                <i className="fas fa-plus-circle plus"></i>{menu}
            </li>);
            contentTextes.push(
                <div className={ "slider-content-text " + ( defaut ? " current" : "" )   } key={`text-${menu}`} id={`text-${label}-${menu}`} >
                    {text}
                </div>
            );
        }

    
        return (<div className="infos">
            <ul>
                {liElements}
            </ul>
            {contentTextes}
        </div>)
    }

    /*
        Ancien code
    <div className="infos">
                            <ul>
                                {listing.map( ({ menu }) => {
                                    return <li key={menu} onMouseOver={(e) => this.Showtext( e , label,  menu ) }>
                                                <i className="fas fa-plus-circle plus"></i>{menu}
                                            </li>
                                })}
                            </ul>
                            {listing.map( ({ defaut = false , menu, text }) => {
                                    return <div className={ "slider-content-text " + ( defaut ? " current" : "" )   } key={`text-${menu}`} id={`text-${label}-${menu}`} >
                                                {text}
                                            </div>
                            })}
                         </div>*/ 

    /**
     * Affiche le texte corresondant à la clé "text-" + label + "-" + menu
     * @param {*} event 
     * @param {*} label 
     * @param {*} menu 
     */                     
    Showtext( { currentTarget }, label, menu ) {
        const key = `text-${label}-${menu}`; 
        const elements = document.querySelectorAll('.slider-content-text');
        [].forEach.call( elements, function(element) {
            if ( element.id === key ) {
                element.classList.add('current');
            } else {
                if( element.classList.contains('current')) {
                    element.classList.remove( 'current' ); 
                }
            }
        }); 
    
    }

    /**
     * Ouvre ou ferme le slide en version bureau
     * Ferme les autres slides
     * @param {*} event
     */
    Slide( {currentTarget} ) {
        if ( !currentTarget.classList.contains( 'extended')) {
            currentTarget.classList.remove("closed"); 
            currentTarget.classList.add('extended'); 
            
            let otherElements = document.querySelectorAll(".slider");
            [].forEach.call( otherElements, function(el) {
                if( el !== currentTarget ) {
                    el.classList.remove( "extended");
                    el.classList.add('closed'); 
                }
            });
        } else {
            let allElements = document.querySelectorAll(".slider");
            [].forEach.call( allElements, function(el) {
                el.classList.remove('closed'); 
                el.classList.remove('extended'); 
            }); 
        }

    }

}
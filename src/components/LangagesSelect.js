import React from 'react';

// import langs datas from datas folder
import { langs } from '../datas/langs.js'; 

export class LangagesSelect extends React.Component {
    constructor( props ) {
        super( props );
        this.state = {
            lang: 'fr',
            opened: false
        }
    }
    render() {
        const { lang,  opened } = this.state; 
       /* let arrow = ( opened ? 
            <i className="fas fa-caret-up" ></i>:
            <i className="fas fa-caret-down" ></i>
        ); */
        
        

        return (<div className={ "lang-select " + (opened? "opened" : "")}>
            <div className="container"  >
                <div className="content" >
                    <div className="flex flex-one flex-row lang-handler" onClick={() => this.Toogle() }>
                        <div className="flex lang-selected"  >
                            <img src={langs[ lang ].flag} alt="" />
                        </div>
                        <div className="flex lang-selected" >
                            {lang}
                        </div>
                        <div className="flex lang-selected lang-arrow"  >
                            <i className="fas fa-caret-down" ></i>    
                            <i className="fas fa-caret-up" ></i>
                        </div>
                    </div>
                    <ul className="listing" >
                        {this.renderListing()}
                    </ul>
                </div>
                
            </div>
        </div>)
    }
    /**
     * rendu du listing des différentes langues
     * @returns {array} list 
     */
    renderListing() {
        const { lang } = this.state;
        let list = [];
        for( let iso in langs ) {
            list.push(
                <li onClick={() => this.Select( iso )} className={"flex flex-one flex-row " +  (iso === lang ? " selected" : "" ) } key={`lang-${iso}`}>
                    <span className="flex flex-one" >
                        <img src={langs[iso].flag} alt=""   />
                    </span>
                    <span className="flex flex-one" >
                        {iso}
                    </span>
                </li>
            );
        }
        return list; 
        
    }

    /**
     * Open or Close the flag list
     */
    Toogle() {
        let opened = this.state.opened;
       this.setState({ 
            opened: ( opened ? false : true )
        })
    }

    /**
     * Select a lang
     * @param {string} lang 
     */
    async Select( lang ) {
        this.setState({
            opened: false,
            lang
        })
    }
}
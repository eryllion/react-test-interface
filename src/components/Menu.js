import React from 'react';
import { LangagesSelect  } from './LangagesSelect.js';

export class Menu extends React.Component {
    constructor( props ) {
        super( props );
        this.state = { };
    }
    render() {
        return (<div id="menu" >
            
            <div id="menu-container" >
                <div id="mobile-menu-handle" onClick={() => this.MenuToggle()}>
                    <i className="fas fa-bars"></i>
                </div>

                <a id="logo" href="/" >
                    <img src={process.env.PUBLIC_URL + '/medias/logo.png'} alt="" /> 
                </a>
                <ul className="menu-list">
                    <li>
                        <a href="/" className="menu-green" >
                            WHO WE ARE
                        </a>
                    </li>
                    <li>
                        <a href="/" className="menu-yellowgreen" >
                           WHAT WE DO
                        </a>
                    </li>
                    <li>
                        <a href="/" className="menu-olive" >
                           HOW WE DO IT
                        </a>
                    </li>
                    <li>
                        <a href="/" className="menu-darkgreen" >
                            BUSINESS UNIT
                        </a>
                    </li>
                    <li>
                        <a href="/" className="menu-seagreen" >
                            FINANCE
                        </a>
                    </li>
                    <li>
                        <a href="/" className="menu-olivedrab" >
                            CARRIÈRE
                        </a>
                    </li>
                    <li>
                        <a href="/" className="menu-darkseagreen" >
                            NATUREX LIVE
                        </a>
                    </li>
                    <li>
                        <a href="/" className="menu-forestgreen" >
                            MEDIA
                        </a>
                    </li>
                    <li>
                        <a href="/" className="menu-mediumseagreen" >
                            CONTACT
                        </a>
                    </li>

                </ul>
                <button className="search-button" ></button>
                <LangagesSelect />
            </div>
            


        </div>)
    }
    /**
     * Open Or Close Mobile Menu
     */
    MenuToggle() {
        const element = document.getElementsByClassName('menu-list')[0];
        if( element.classList.contains( 'open') ) {
            element.classList.remove('open');
        } else {
            element.classList.add('open'); 
        }
    }
}

import React from 'react';  
const { PUBLIC_URL  } = process.env; 

export class Header extends React.Component {
    constructor( props)  {
        super( props );
        this.state = {
            playing: false
        };
    }
    render() {
        
        const { playing } = this.state; 
        return (<div id="header" className={(playing ? "playing" : "")}  >
            <div className="video-container" >
                {this.renderVideo()}
            </div>
            <div className="header-mask" >
                <div className="header-mask-content" >
                   <div className="zones" >

                   </div>
                   <div className="zone-command" >
                        <div className="command" >
                            <span onClick={() => this.Play()} className="play"></span>
                            <span className="play-video-text" >LIRE LA VIDÉO</span>
                            <span className="chevron-down" ></span>
                            <span className="scroll-text" >Scrollez pour naviguer</span>
                        </div>
                   </div>
                   <div className="zones" >
                       <div className="header-halo4" >
                            <img src={`${PUBLIC_URL}/medias/header-text.png`} alt="" />
                       </div>
                   </div>
                </div>
            </div>
        </div>)
    }
    /** rendu de l'élément Video IFrame */
    renderVideo() {
        if( !this.state.playing) return false; 
        const { offsetHeight , offsetWidth } = document.getElementById('header');
        return (<iframe width={offsetWidth} height={offsetHeight} 
            src="https://www.youtube.com/embed/ORce3K6ruKw" title="YouTube video player" frameborder="0" 
            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" alowfullscreen></iframe>)
    }
    
    /**
     * Close the header-mask (with css rules) and draw iframe
     */
    Play() {
        this.setState({ playing: true }); 
    }
}


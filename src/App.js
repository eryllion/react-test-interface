import { Header, Menu, Sliders } from './components/index.js'; 


function App() {
  return (
    <div id="main" >
        <Header />
        <Menu />
        <Sliders />

        <div id="other-block" >
          ESPACE VIDE
          <br />
          Pour démontrer et tester que le menu sticky fonctionne bien 
        </div>
        
    </div>
  );
}

export default App;
